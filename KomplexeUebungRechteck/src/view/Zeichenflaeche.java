package view;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;
import controller.BunteRechteckeController;
import model.Rechteck;

public class Zeichenflaeche extends JPanel {

	private final BunteRechteckeController CONTROLLER;

	public Zeichenflaeche(BunteRechteckeController brc) {
		CONTROLLER = brc;
		// paintComponent(getGraphics());
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		this.setBackground(Color.WHITE);
		g.setColor(Color.BLACK);
		g.drawRect(0, 0, 50, 50);

		for (int i = 0; i < CONTROLLER.getRechtecke().size(); i++) {
			g.drawRect(CONTROLLER.getRechtecke().get(i).getPunkt().getX(), CONTROLLER.getRechtecke().get(i).getPunkt().getY(),
					CONTROLLER.getRechtecke().get(i).getBreite(), CONTROLLER.getRechtecke().get(i).getHoehe());
		}

	}

}
