package view;

import controller.BunteRechteckeController;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.Punkt;
import model.Rechteck;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;

public class RechteckEingabe extends JFrame {
	
	//private Rechteck r = new Rechteck();
	private BunteRechteckeController brc = new BunteRechteckeController();
	private JPanel contentPane;
	private JTextField tfdVariableX;
	private JTextField tfdVariableY;
	private JTextField tfdVariableBreite;
	private JTextField tfdVariableHoehe;
	private JButton btnAddRectangle;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RechteckEingabe frame = new RechteckEingabe();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RechteckEingabe() {
		setTitle("Rechteck");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel pnlBorder = new JPanel();
		contentPane.add(pnlBorder, BorderLayout.CENTER);
		pnlBorder.setLayout(new BorderLayout(0, 0));
		
		JPanel pnlBorderLeft = new JPanel();
		pnlBorder.add(pnlBorderLeft, BorderLayout.NORTH);
		pnlBorderLeft.setLayout(new BorderLayout(0, 0));
		
		JLabel lblEingabe = new JLabel("Geben sie hier ihre Rechteck werte ein:");
		lblEingabe.setHorizontalAlignment(SwingConstants.CENTER);
		lblEingabe.setEnabled(true);
		pnlBorderLeft.add(lblEingabe, BorderLayout.NORTH);
		
		JPanel panel_1 = new JPanel();
		pnlBorderLeft.add(panel_1, BorderLayout.SOUTH);
		panel_1.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		tfdVariableX = new JTextField();
		tfdVariableX.setText("x");
		panel_1.add(tfdVariableX);
		tfdVariableX.setColumns(10);
		
		tfdVariableY = new JTextField();
		tfdVariableY.setText("y");
		panel_1.add(tfdVariableY);
		tfdVariableY.setColumns(10);
		
		tfdVariableBreite = new JTextField();
		tfdVariableBreite.setText("breite");
		panel_1.add(tfdVariableBreite);
		tfdVariableBreite.setColumns(10);
		
		tfdVariableHoehe = new JTextField();
		tfdVariableHoehe.setText("hoehe");
		panel_1.add(tfdVariableHoehe);
		tfdVariableHoehe.setColumns(10);
		
		JPanel panel_2 = new JPanel();
		pnlBorder.add(panel_2, BorderLayout.SOUTH);
		
		JButton btnAddRectangle = new JButton("Rechteck Hinzufügen");
		panel_2.add(btnAddRectangle);
		
		thehandler handler = new thehandler();
		btnAddRectangle.addActionListener(handler);
		
	}
	
	private class thehandler implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == btnAddRectangle) {
				int x = Integer.parseInt(tfdVariableX.getText());
				int y = Integer.parseInt(tfdVariableY.getText());
				int breite = Integer.parseInt(tfdVariableBreite.getText());
				int hoehe = Integer.parseInt(tfdVariableHoehe.getText());
				
				Rechteck r = new Rechteck(x,y,breite,hoehe);
				
				brc.add(r);
				
			}
			
		}
		
	}

}
