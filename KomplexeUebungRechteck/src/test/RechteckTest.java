package test;

import model.Rechteck;
import model.Punkt;
import controller.BunteRechteckeController;

public class RechteckTest {

	public static void main(String[] args) {
		
		/*
		Rechtecke
		*/

		// Parameter: int x, int y, int breite, int hoehe

		// Parametrisierte Rechtecke

		Punkt p0 = new Punkt(200,200);
		Rechteck rechteck5 = new Rechteck(p0.getX(),p0.getY(), 200, 200);
		Punkt p1 = new Punkt(800,400);
		Rechteck rechteck6 = new Rechteck(p1.getX(),p1.getY(),20,20);
		Punkt p2 = new Punkt(800,450);
		Rechteck rechteck7 = new Rechteck(p2.getX(),p2.getY(),20,20);
		Punkt p3 = new Punkt(850, 400);
		Rechteck rechteck8 = new Rechteck(p3.getX(),p3.getY(),20,20);
		Punkt p4 = new Punkt(855,455);
		Rechteck rechteck9 = new Rechteck(p4.getX(),p4.getY(),25,25);

		// Unparametrisierte Rechtecke

		Rechteck rechteck0 = new Rechteck();
		rechteck0.setPunkt(new Punkt(10, 10));
		rechteck0.setBreite(30);
		rechteck0.setHoehe(40);

		Rechteck rechteck1 = new Rechteck();
		rechteck1.setPunkt(new Punkt(25,25));
		rechteck1.setBreite(100);
		rechteck1.setHoehe(20);

		Rechteck rechteck2 = new Rechteck();
		rechteck2.setPunkt(new Punkt(260,10));
		rechteck2.setBreite(200);
		rechteck2.setHoehe(100);

		Rechteck rechteck3 = new Rechteck();
		rechteck3.setPunkt(new Punkt(5,500));
		rechteck3.setBreite(300);
		rechteck3.setHoehe(25);

		Rechteck rechteck4 = new Rechteck();
		rechteck4.setPunkt(new Punkt(100,100));
		rechteck4.setBreite(100);
		rechteck4.setHoehe(100);

		// Ausgaben:

		System.out.println("Rechteck 0: x: " + rechteck0.getPunkt().getX() + " y: " + rechteck0.getPunkt().getY() + " breite: "
				+ rechteck0.getBreite() + " höhe: " + rechteck0.getHoehe());
		
		System.out.println("Rechteck 1: x: " + rechteck1.getPunkt().getX() + " y: " + rechteck1.getPunkt().getY() + " breite: "
				+ rechteck1.getBreite() + " höhe: " + rechteck1.getHoehe()); 
		
		System.out.println("Rechteck 2: x: " + rechteck2.getPunkt().getX() + " y: " + rechteck2.getPunkt().getY() + " breite: "
				+ rechteck2.getBreite() + " höhe: " + rechteck2.getHoehe());
		
		System.out.println("Rechteck 3: x: " + rechteck3.getPunkt().getX() + " y: " + rechteck3.getPunkt().getY() + " breite: "
				+ rechteck3.getBreite() + " höhe: " + rechteck3.getHoehe());
		
		System.out.println("Rechteck 4: x: " + rechteck4.getPunkt().getX() + " y: " + rechteck4.getPunkt().getY() + " breite: "
				+ rechteck4.getBreite() + " höhe: " + rechteck4.getHoehe());
		
		System.out.println("Rechteck 5: x: " + rechteck5.getPunkt().getX() + " y: " + rechteck5.getPunkt().getY() + " breite: "
				+ rechteck5.getBreite() + " höhe: " + rechteck5.getHoehe());
		
		System.out.println("Rechteck 6: x: " + rechteck6.getPunkt().getX() + " y: " + rechteck6.getPunkt().getY() + " breite: "
				+ rechteck6.getBreite() + " höhe: " + rechteck6.getHoehe());
		
		System.out.println("Rechteck 7: x: " + rechteck7.getPunkt().getX() + " y: " + rechteck7.getPunkt().getY() + " breite: "
				+ rechteck7.getBreite() + " höhe: " + rechteck7.getHoehe());
		
		System.out.println("Rechteck 8: x: " + rechteck8.getPunkt().getX() + " y: " + rechteck8.getPunkt().getY() + " breite: "
				+ rechteck8.getBreite() + " höhe: " + rechteck8.getHoehe());
		
		System.out.println("Rechteck 9: x: " + rechteck9.getPunkt().getX() + " y: " + rechteck9.getPunkt().getY() + " breite: "
				+ rechteck9.getBreite() + " höhe: " + rechteck9.getHoehe());

		System.out.println(rechteck0.toString().equals( "Rechteck [x=10, y=10, breite=30, hoehe=40]"));
		
		System.out.println(rechteck0.toString());
		
		
		/*
			Controller
		*/
		
		BunteRechteckeController controller = new BunteRechteckeController();
		controller.add(rechteck0);
		controller.add(rechteck1);
		controller.add(rechteck2);
		controller.add(rechteck3);
		controller.add(rechteck4);
		controller.add(rechteck5);
		controller.add(rechteck6);
		controller.add(rechteck7);
		controller.add(rechteck8);
		controller.add(rechteck9);
		
		System.out.println(controller.toString());
		System.out.println(controller.toString().equals("BunteRechteckeController [rechtecke=[Rechteck [x=10, y=10, breite=30, hoehe=40], Rechteck [x=25, y=25, breite=100, hoehe=20], Rechteck [x=260, y=10, breite=200, hoehe=100], Rechteck [x=5, y=500, breite=300, hoehe=25], Rechteck [x=100, y=100, breite=100, hoehe=100], Rechteck [x=200, y=200, breite=200, hoehe=200], Rechteck [x=800, y=400, breite=20, hoehe=20], Rechteck [x=800, y=450, breite=20, hoehe=20], Rechteck [x=850, y=400, breite=20, hoehe=20], Rechteck [x=855, y=455, breite=25, hoehe=25]]]"));
		// Eclipse hat hier automatisch alles in eine Zeile geschrieben während in der Moodle ankündigung die erwartete Ausgabe über mehrere Zeilen geht, dementsprechend könnte es falsch sein sollte es aber nicht.
		
		//HA08
		Rechteck eck10 = new Rechteck(-4,-5,-50,-200);
	    System.out.println(eck10); //Rechteck [x=-4, y=-5, breite=50, hoehe=200]
	    Rechteck eck11 = new Rechteck();
	    eck11.setPunkt(new Punkt(-10,-10));
	    eck11.setBreite(-200);
	    eck11.setHoehe(-100);
	    System.out.println(eck11);//Rechteck [x=-10, y=-10, breite=200, hoehe=100]
		//HA11 test um für HA12 fortzufahren
	    System.out.println(rechteck0.enthaelt(10,10));
	    System.err.println(rechteck0.enthaelt(rechteck0));
	    
	    //HA13
	    Rechteck rrechteck0 = new Rechteck();
	    rrechteck0.generiereZufallsRechteck();
	    System.out.println(rrechteck0);
	    
	    
	    //HA14
	    System.out.println(rechteckeTesten());
	    
	}
	
	public static boolean rechteckeTesten() {
		boolean x = false;
		Rechteck flaeche = new Rechteck(0, 0, 1200, 1000);
		Rechteck[] rechtecke = new Rechteck[50000];
		for (int i = 0; i < rechtecke.length; i++) {
			rechtecke[i] = new Rechteck();
			rechtecke[i].generiereZufallsRechteck();
			if (flaeche.enthaelt(rechtecke[i])) {
				System.out.println(i);
				x = true;
			}
			if (!(flaeche.enthaelt(rechtecke[i])))
				return false;
		}
		System.out.println(rechtecke.length);
	System.out.println("x");	
	return x;
	}
	
	
}
