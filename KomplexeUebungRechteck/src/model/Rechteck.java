package model;

import java.lang.Math;
import java.util.Random;
import model.Punkt;

public class Rechteck {

	// attribute
	private Punkt p = new Punkt();
	private int breite = checkValue(getBreite());
	private int hoehe = checkValue(getHoehe());

	// Konstruktoren
	public Rechteck() {
		this.p.setX(0);
		this.p.setY(0);
		this.breite = 0;
		this.hoehe = 0;
	}

	public Rechteck(int x, int y, int breite, int hoehe) {
		this.p.setX(x);
		this.p.setY(y);
		this.breite = checkValue(breite);
		this.hoehe = checkValue(hoehe);
	}
	// Verwaltungsmethoden

	public int getBreite() {
		return breite;
	}

	public void setBreite(int breite) {
		this.breite = checkValue(breite);

	}

	public int getHoehe() {
		return hoehe;
	}

	public void setHoehe(int hoehe) {
		this.hoehe = checkValue(hoehe);

	}

	public void setPunkt(Punkt p) {
		this.p = p;
	}

	public Punkt getPunkt() {
		return this.p;
	}

	@Override
	public String toString() {
		return "Rechteck [p=" + p + ", breite=" + breite + ", hoehe=" + hoehe + "]";
	}

	public int checkValue(int zahl) {
		return Math.abs(zahl);
	}

	public boolean enthaelt(int x, int y) {
		return drin(x, y);
	}

	//HA11
	public boolean enthaelt(Punkt p) {
		return drin(p.getX(), p.getY());
	}

	private boolean drin(int x, int y) {
		return breite > 0 && hoehe > 0
					&& x >= this.p.getX() && x < this.p.getX() + breite
					&& y >= this.p.getY() && y < this.p.getY() + hoehe;
	}
	//HA12
	public boolean enthaelt(Rechteck rechteck) {
		return breite > 0 && hoehe > 0 && rechteck.getBreite() > 0 && rechteck.getHoehe() > 0
				&& rechteck.getPunkt().getX() >= this.p.getX() && rechteck.getPunkt().getX() + rechteck.getBreite() <= this.p.getX() + this.breite
				&& rechteck.getPunkt().getY() >= this.p.getY() && rechteck.getPunkt().getY() + rechteck.getHoehe() <= this.p.getY() + this.hoehe;
	}
	
	public void generiereZufallsRechteck() {
		Random rand = new Random();
		this.p.setX(rand.nextInt(1200));
		this.p.setY(rand.nextInt(1000));
		int x = rand.nextInt(1200);
		if (x > this.p.getX()) 
			this.breite = x - this.p.getX();
		else
			this.breite = this.p.getX() - x;
		x = rand.nextInt(1000);
		if (x > this.p.getY())
			this.hoehe = x - this.p.getY();
		else
			this.hoehe = this.p.getY() - x;
	}
	
	public static Rechteck generiereZufallsRechteck2() {
		Random rand = new Random();
		int x = rand.nextInt(1200);
		int y = rand.nextInt(1000);
		int new_breite = rand.nextInt(x);
		int new_hoehe = rand.nextInt(y);
		
		return new Rechteck(x, y, new_breite, new_hoehe);
	}

}
