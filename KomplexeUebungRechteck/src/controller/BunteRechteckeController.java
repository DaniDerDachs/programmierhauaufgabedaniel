package controller;

import java.util.LinkedList;
import java.util.Random;

import model.Punkt;
import model.Rechteck;

public class BunteRechteckeController {
	//attribute
	private LinkedList<Rechteck> rechtecke = new LinkedList<Rechteck>(); //leichter als mit normalem Array, da sowieso mit LinkedList gearbeitet werden soll
	Rechteck r = new Rechteck();
	
	public static void main(String[] args) {
	} //leere Main

	public BunteRechteckeController() {
		rechtecke = new LinkedList<Rechteck>();
	}
	
	public void add(Rechteck rechteck) {
		rechtecke.add(rechteck);
	}
	
	public void reset() {
		rechtecke.clear();
	}

	public LinkedList<Rechteck> getRechtecke() {
		return rechtecke;
	}

	@Override
	public String toString() {
		return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
	}
	
	public void generiereZufallsRechtecke(int anzahl) {
		reset();
		for (int i = 0; i < anzahl; i++) {
			Random rand = new Random();
			int x = rand.nextInt(1200);
			int y = rand.nextInt(1200);
			r.setPunkt(new Punkt(x,y));
			r.setBreite(rand.nextInt(x));
			r.setHoehe(rand.nextInt(y));
			add(r);
		}
	}
	
}
