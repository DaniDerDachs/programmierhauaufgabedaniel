Das Model-View-Controller Model ist ein Konzept welches aus drei teilen bestehen. 
Das Modell hat die gesamte Logik des Projekts und auch die Anbindung an die Datenbank, die wir derzeitig noch nicht haben.
Die View Klasse kümmert sich um die Präsentation und Nutzeroberfläche. Das haben wir auch in unserem Projekt bereits umgesetzt.
Der Controller ist der Kopf des gesamten Konzepts. dieser übernimmt die Steuerung und Vermittlung.
Da der Controller durch dieses Konzept viel machen muss, also gewollte und ungewollte Szenarien organisieren, sollte dieser möglichst wenig Code ausführen.
Das Entwurfsmuster selbst sieht vor, dass der Controller keine Daten hält.

Vorteile des Modells sind die übersichtlichkeit und einfachkeit. Dieses Konstrukt ist nicht zu komplex und jeder part des Modells kennt seine Rolle.
MVC hat sich soweit verändert, dass man einen Controller hat der an jede URL angebunden ist. das lässt dem Controller viel Arbeit verrichten.
Durch die Verbindung von einem Controller mit vielen Models und "views" ist das Ursprüngliche MVC Konzept teilweise "gescheitert" beziehungsweise nicht erfüllt.
Es wurde ein "Front-Controller-Pattern" Erfunden welcher die Daten von view oder Model an einen Controller geleitet hat welcher dieses dann zum Hauptcontroller leiten musste.
Dieses Konzept, welches als Pattern gedacht war und nun eine ganze Architektur ist, ist nun komplexer den je und nicht "inter-programmiersprachenfähig". Als Beispiel werden JSON dateien von Java-Script als Models erkannt.
Was bei manchen Sprachen als view gedacht war. Aus solchen Konzepten folgten neue Modelle wie "MVVM". Aus einem Pattern wurde plötzlich eine Architektur was so Ursprünglich nicht gedacht war. Aus der hohen Komplexität folgten verückte Umsetzungen.
Router wurden zu Controllern und so weiter. Das hängt dann immer vom Framework(Ein Framework ist ein Programmiergerüst, das in der Softwaretechnik, insbesondere im Rahmen der objektorientierten Softwareentwicklung 
sowie bei komponentenbasierten Entwicklungsansätzen, verwendet wird. Im allgemeineren Sinne bezeichnet man mit Framework auch einen Ordnungsrahmen.) ab.




Quellen:
https://www.youtube.com/watch?v=VGQMQUa_o5o
https://www.youtube.com/watch?v=1K-YuLzqyEg
https://de.wikipedia.org/wiki/Framework